<div align="center">
<h3 align="center">Visual Impairment Navigation Assistant</h3>

  <p align="center">
    An accessibility script for VRChat intended for use with visually impaired players
  </p>
</div>

## About VIN-A

VIN-A is a simple script that aims to bring non-visual collider-based navigation to VRChat in order to improve accessibility among visually impaired people. Current features include:

 * **Per-layer workflow:** only detect colliders sharing the same GameObject
 * **Proximity-driven audio & haptic feedback:** amplitude & frequency increases based on your proximity to the collider
 * **Directional audio support:** the source snaps to the surface point, allowing for haptic vest integration
 * **AmbiX/CLCRLR support:** using collision mesh's normal you can "rotate" point-directional audio accordingly
 * **Per-component workflow:** each tracking system only tracks one input point


### Dependencies

* [UdonSharp](https://udonsharp.docs.vrchat.com/setup)




## Getting Started

Before you can import the script into your world, there are some things you have to familiarise yourself with when it comes to level design.

In order for this solution to work well, you have to design your soundscape much like you would design your visual soundscape. That normally means placing points of interest ("landmarks") in key locations of your scene. While it may prove difficult to design and mix down the spatial audio, you indeed can create a separate layer of accessibility sounds only audible when the system is active.

Please, refer to the [demonstration scene](https://vrchat.com/home/world/wrld_57398ba9-92d7-4ce6-9398-06711bfa68c6) to get acquainted with a basic audio landmarking principle.


### Prerequisites

 * Install VRCSDK3 for Worlds
 * Install UdonSharp and its dependencies


### Installation

1. Download the latest release available
2. Import the unitypackage archive into your unity project
3. Locate the `VisualImpairmentNavigationAssistant` prefab under `/Alareis/Content/Tests/VisionImpairedFeedback`
4. Add the prefab to your scene
5. Under `VisualImpairmentNavigationAssistant/Udon`, in each child GameObject configure the parameters to your liking  
   Provided parameters are as presented in the demo world
6. Assign a desired layer to each individual object under `VisualImpairmentNavigationAssistant/Udon`  
   **Be advised:** VIN-A only finds colliders sharing the same layer!
7. Assign your environmental colliders accordingly




## Usage

Once player loads into the scene, VIN-A will automatically start polling the environment. If this isn't the desired behaviour, you can disable the root prefab to completely disable the system until it's awaken again.


### Custom sounds

You can customize the sounds used by the system. It is implied that the sounds will be pitch- and amplitude-corrected based on the player's distance, hence a steady/regular tone should be used. Shorter clips are prefered, since the audio is meant to be looped.

Each individual audio source is located in `VisualImpairmentNavigationAssistant/TrackPulse/<tracking point>/Pulse`, where `<tracking point>` stands for Head, Hand L, or Hand R respectively.

The Pulse GameObject contains an AudioSource thar references the desired AudioClip. You can replace it in corresponding fields.


### Haptic feedback

It is highly suggested that you enable haptic feedback. While it is going to drain controllers' batteries significantly faster, haptic feedback allows players to figure out their virtual proximity to the colliders significantly faster than by only using the sound.

**Be aware:** different controllers receive haptic signals differently. Existing values are recommended as they are based on various controllers, such as Vive Wands, various Oculus controllers, Valve's Knuckles.


### Point-directional audio

An option labeled `Target Rotation Use Raycast` allows you to orient the tracking points based on the mesh collider's normals. This, in turn, allows us to integrate point-directional or ambisonic audio and align it to the collider's surface. Additionally, you can provide high-contrast directional markers for hard of sight players.

It is important to note that this approach uses ray casting on every frame, albeit only to the closest point. In theory, this may not scale so well, so, only use this option if you actually need normal information.




## Contact

Lars Sørensen - [@alareis_vrc](https://twitter.com/alareis_vrc) - [alareis](https://vrchat.com/home/user/usr_e3283b19-0468-479f-82de-0907c41920b0)

Project Link: [https://gitlab.com/Rikketh/VIN-A](https://gitlab.com/Rikketh/VIN-A)




## Acknowledgments

### Testers
TKPerson, X man 20000, NamelessJudge, Volmun