﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace Alareis.VRC.General
{
	public class TrackingDataTracker : UdonSharpBehaviour
	{
		public Transform target;
		public VRCPlayerApi.TrackingDataType trackPoint;

		void Start()
		{
			if (!this.target) {
				this.target = this.gameObject.transform;
			}
		}

		private void Update()
		{
			VRCPlayerApi player = Networking.LocalPlayer;

			Vector3 pos = player.GetTrackingData(trackPoint).position;
			Quaternion rot = player.GetTrackingData(trackPoint).rotation;

			this.target.SetPositionAndRotation(pos, rot);
		}
	}
}