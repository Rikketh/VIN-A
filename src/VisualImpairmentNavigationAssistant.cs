﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

namespace Alareis.VRC.VisualImpairmentNavigationAssistant
{
	[UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
	public class VisualImpairmentNavigationAssistantController : UdonSharpBehaviour
	{
		[Header("VIN-A: Accessibility Controller")]

		[Tooltip("A transform that will be used as the distance basis")]
		public Transform source;

		[Tooltip("A transform that will be snapped to the closest collision point")]
		public Transform target;

		[Tooltip("Raycast-get collision mesh normal for AmbiX or directionality")]
		public bool targetRotationUseRaycast = false;

		[Tooltip("Minimum search distance")]
		public float distanceMin = 0.025f;

		[Tooltip("Maximum search distance")]
		public float distanceMax = 1.0f;

		[Tooltip("Audio source that will play a clip")]
		public AudioSource audioSource;

		[Tooltip("Audio source's volume coefficient")]
		public float volumeMultiplier = 0.5f;

		public bool sendHapticToLeftHand = false;
		public bool sendHapticToRightHand = false;

		public float hapticDuration = 0.1f;
		public float hapticAmplitude = 0.75f;
		public float hapticFrequency = 0.5f;

		[Tooltip("A position tracking points will be returned to if no collider is found")]
		public Transform restPosition;

		private int layer;

		private VRCPlayerApi playerLocal;
		private VRC_Pickup.PickupHand pickupHandRight;
		private VRC_Pickup.PickupHand pickupHandLeft;

		void Start()
		{
			// we only want to find colliders with the same layer as the controller itself
			this.layer = this.gameObject.layer;

			// reference hands
			playerLocal = Networking.LocalPlayer;
			pickupHandRight = VRC_Pickup.PickupHand.Right;
			pickupHandLeft = VRC_Pickup.PickupHand.Left;

			if (!this.restPosition) {
				this.restPosition = this.gameObject.transform;
			}
		}

		void Update()
		{
			// creates a sphere with the radius of distanceMax that collects all colliders
			// that share the same layer as the gameObject with this component attached to it
			Collider[]	colliders = Physics.OverlapSphere(
														this.source.transform.position,
														this.distanceMax,
														1 << this.layer
									);

			// set up per-frame decaying distance buffer
			float		bestDistance = float.MaxValue;
			Vector3		closestPoint = this.restPosition.position;

			foreach (Collider c in colliders)
			{
				// check where the closest point on the collision mesh is
				// in regards to our arbitrary target point
				closestPoint = c.ClosestPoint(this.source.transform.position);

				// now find how far away are we from that point
				float distance = Vector3.Distance(this.source.transform.position, closestPoint);

				// if said point is too close, forget about it
				if (distance < this.distanceMin) {
					break;
				}

				// otherwise consider it the best candidate
				if (distance < bestDistance) {
					bestDistance = distance;
				}
			}

			// now that we have our closest point through all the candidates, it's time to actually do stuff

			// create a normalised distance driver based on our [min,max] remap
			float clampedDistance = Mathf.InverseLerp(this.distanceMax, this.distanceMin, bestDistance);

			// if we use audio cues, also set up its rotation based on inverted collision mesh
			// normal to represent the direction of ambisonic sounds
			if (this.audioSource) {
				this.target.transform.position = closestPoint;

				// if we want to test for surface normal of the collision mesh
				if (this.targetRotationUseRaycast) {
					// we have to construct a ray metadata
					Vector3 direction = (closestPoint - this.source.transform.position).normalized;
					RaycastHit hit;
					Ray ray = new Ray(this.source.transform.position, direction); // from our source towards the point

					if (Physics.Raycast(ray, out hit, this.distanceMax, 1 << this.layer))
					{
						// if a collider is within the maximum range on the same layer, match its surface normal
						this.target.transform.rotation = Quaternion.LookRotation(hit.normal);;
					}
				}

				this.audioSource.pitch = clampedDistance;
				this.audioSource.volume = clampedDistance * this.volumeMultiplier;
			}

			if (this.sendHapticToLeftHand && clampedDistance > 0) {
				this.playerLocal.PlayHapticEventInHand(
												this.pickupHandLeft,
												this.hapticDuration,
												this.hapticAmplitude * clampedDistance,
												this.hapticFrequency * clampedDistance
				);
			}

			if (this.sendHapticToRightHand && clampedDistance > 0) {
				this.playerLocal.PlayHapticEventInHand(
												this.pickupHandRight,
												this.hapticDuration,
												this.hapticAmplitude * clampedDistance,
												this.hapticFrequency * clampedDistance
				);
			}
		}
	}
}